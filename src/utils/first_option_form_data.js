import { useState } from 'react'

export function useFirstOptionFormData() {
  const [vmName, setVmData] = useState('')
  const [clusterName, setClusterName] = useState('')
  const [os, setOS] = useState('windows')

  const userFieldList = [
    {
      label: 'Virtual Machine Name',
      placeHolder: 'name',
      onChange: e => setVmData(e.target.value),
    },
    {
      label: 'Cluster Name',
      placeHolder: 'NFV',
      onChange: e => setClusterName(e.target.value),
    },
  ]

  const valueList = [
    { value: 'windows', displayText: 'Windows' },
    { value: 'linux', displayText: 'Linux' },
  ]

  const userSelectInputFieldData = {
    valueList,
    label: 'Select Platform',
    onChange: selectedValue => setOS(selectedValue),
  }

  return {
    userData: {
      vmName,
      clusterName,
      os,
    },
    formData: {
      userFieldList,
      userSelectInputFieldData,
    },
  }
}
