import React, { useState } from 'react'
import { Layout, Menu, Icon } from 'antd'
import { Link, withRouter } from 'react-router-dom'

export const SideMenu = withRouter(props => {
  const { location } = props
  const { Sider } = Layout
  const [collapsed, setCollased] = useState(false)
  const menuItemList = [
    {
      displayText: `Create Virtual Machine`,
      link: `/create-vm`,
    },
  ]

  return (
    <Sider
      collapsible
      collapsed={collapsed}
      onCollapse={() => setCollased(!collapsed)}
      width={300}
    >
      <div className='logo' />
      <Menu
        theme='dark'
        defaultSelectedKeys={[location.pathname]}
        mode='inline'
      >
        {menuItemList.map(menuItem => (
          <Menu.Item key={menuItem.link}>
            <Link to={menuItem.link}>
              <Icon type='info-circle' />
              <span>{menuItem.displayText}</span>
            </Link>
          </Menu.Item>
        ))}
      </Menu>
    </Sider>
  )
})
