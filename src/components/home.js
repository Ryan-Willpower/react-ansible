import React from 'react'
import { Layout } from 'antd'

import { SideMenu } from './side_menu'
import { FirstOptionContent } from './contents/first_option'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

export const Home = () => {
  const { Content } = Layout

  return (
    <Layout style={{ minHeight: `100vh` }}>
      <Router>
        <SideMenu />

        <Content>
          <Switch>
            <Route path='/create-vm'>
              <FirstOptionContent />
            </Route>
          </Switch>
        </Content>
      </Router>
    </Layout>
  )
}
