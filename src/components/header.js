import React from 'react'
import { Layout } from 'antd'

const { Header } = Layout

export function PageHeader() {
  return <Header style={{ background: '#f1f2f5', padding: 0 }} />
}
