import React from 'react'

export function FormTitle({ children }) {
  return <div style={{ marginBottom: '30px' }}>{children}</div>
}
