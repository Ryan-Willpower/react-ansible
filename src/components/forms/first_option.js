import React from 'react'
import { Form, Button } from 'antd'

import { useFirstOptionFormData } from '../../utils/first_option_form_data'

import { UserInput, UserSelectInput } from '../user_input'

export function FirstOptionFormField() {
  // @todo sent this user data to API
  const { userData, formData } = useFirstOptionFormData()
  const { userFieldList, userSelectInputFieldData } = formData

  return (
    <Form layout='vertical'>
      {userFieldList.map(fieldData => (
        <UserInput options={fieldData} key={fieldData.placeHolder} />
      ))}
      <UserSelectInput options={userSelectInputFieldData} />
      <Button type='primary' size='large' style={{ marginTop: `20px` }}>
        Create VM
      </Button>
    </Form>
  )
}
