import React from 'react'
import { Input, Select } from 'antd'

export function UserInput({ options }) {
  const { placeHolder, label, onChange } = options

  return (
    <div style={{ marginBottom: `20px` }}>
      <label>
        {label}
        <Input
          style={{ marginTop: `10px` }}
          placeholder={placeHolder}
          onChange={onChange}
        />
      </label>
    </div>
  )
}

export function UserSelectInput({ options }) {
  const { valueList, label, onChange } = options
  const { Option } = Select

  return (
    <div>
      <label>
        {label}
        <Select
          style={{ marginTop: `10px` }}
          defaultValue={valueList[0].value}
          onChange={onChange}
        >
          {valueList.map(data => (
            <Option value={data.value} key={data.value}>
              {data.displayText}
            </Option>
          ))}
        </Select>
      </label>
    </div>
  )
}
