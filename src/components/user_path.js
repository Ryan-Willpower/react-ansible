import React from 'react'
import { Breadcrumb } from 'antd'

const pathList = ['user', 'Bill']

export function UserPath() {
  return (
    <Breadcrumb style={{ margin: `16px 0` }}>
      {pathList.map(path => (
        <Breadcrumb.Item key={path}>{path}</Breadcrumb.Item>
      ))}
    </Breadcrumb>
  )
}
