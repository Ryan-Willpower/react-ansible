import React from 'react'
import { Col, Row } from 'antd'

import { FirstOptionFormField } from '../forms/first_option'
import { FormTitle } from '../forms/title'

import './first_option.css'

export function FirstOptionContent() {
  return (
    <>
      <div className='bg-white p-25'>
        <FormTitle>Create virtual machine on vCenter</FormTitle>
        <Row>
          <Col span={8}>
            <FirstOptionFormField />
          </Col>
        </Row>
      </div>
    </>
  )
}
